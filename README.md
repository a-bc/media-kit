## Academy of Behavior Change Media Kit <img style="border:0px" align="right" width="200" height="200" src="logos/a-bc--logo--text--white-bg--800px.png" />

This is the media kit of the Academy of Behavior Change. All these files are licensed under CC-BY-NC-SA.

The A-BC colors are:

`#e1f0ffff`
`#87c3ffff`
`#0055acff`
`#002f5eff`

![Creative Commons badge](/readme-resources/by-nc-sa.eu.png "CC-BY-NC-SA")

